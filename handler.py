import os
import json
import hashlib
import hmac

import requests
from lambda_decorators import load_json_body


def webhook(event, context):
    assert (
        hmac.new(
            os.environ.get("HMAC_SECRET", "").encode(),
            event["body"].encode(),
            digestmod=hashlib.md5,
        ).hexdigest()
        == event["headers"]["X-OurStreets-Signature"]
    ), "HMAC signature must match"

    body = json.loads(event["body"])

    requests.post(
        os.environ["SLACK_WEBHOOK_URL"],
        json={"text": f"https://l.ourstreets.co/{body['hashedReportId']}"},
    )

    return {"statusCode": 200, "body": ""}

# Slack OurStreets Webhook Forwarder

This is a simple webhook handler for forwarding webhook notifications to slack.

It is implemented in Python and deployed to AWS Lambda using Serverless.

## Deploy
1. [Create a slack custom webhook integration](https://slack.com/apps/A0F7XDUAZ-incoming-webhooks)
1. Choose/create an HMAC secret
1. Install serverless if you have not yet: `npm i -g serverless`
1. Install the project dependencies: `npm i`
1. Deploy, setting the two environment variables to your slack webhook URL and
   the HMAC secret you created
```
SLACK_WEBHOOK_URL=https://example.com HMAC_SECRET=secret sls deploy
```
1. [Create a new OurStreets Webhook Integration](https://ourstreets.app/integrations/new).
   Setting the URL to the one output in the deploy step and the secret to the one
   created or chosen in step 2.